"""
This Python Script realizes and tries to explore the rank-based Markov methods 

Created at Tue Feb. 03 16:23:01 2014
Revised at Tue May. 08 00:02:04 2014
Author: Sagus Wang
"""

import sys, os
import copy
import pysal
import numpy as np
import subprocess as sp
import collections
from pysal.contrib.viz import mapping

from PySide.QtCore import *
from PySide.QtGui import *

import matplotlib
import matplotlib.patches as mpatches
import matplotlib.text as mtxt
from matplotlib.collections import PatchCollection
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QTAgg as NavigationToolbar
from matplotlib.figure import Figure
#----------------------------------------------------------------------------------
#----------------------------------------------------------------------------------
class MapWindow(QMainWindow):
	'''
	This class make a window to contain and show choropleth maps.
	'''
	def __init__(self, shpFile, markerKeyName = None, parent=None):
		QMainWindow.__init__(self, parent)
		self.setWindowTitle('First Mean Passage Time Demo')
		self.move(30, 30)

		self.shp = pysal.open(shpFile + '.shp')
		self.map = mapping.map_poly_shp(self.shp)
		self.dbf = pysal.open(shpFile + '.dbf')

		centers = [i.centroid for i in self.shp]

		if markerKeyName is not None:
			txtSize = 1.2
			n = len(self.shp)
			satxt = self.dbf.by_col[markerKeyName]
			satxt = [self.shp[i].area > 5 and self.dbf.by_col[markerKeyName][i] or '' for i in range(n)]
			self.textMarker  = PatchCollection(
				[mpatches.PathPatch(mtxt.TextPath(np.array(xy) - (txtSize / 2), txt, size=txtSize)) for xy, txt in zip(centers, satxt) if len(txt) > 0],
				edgecolor='white', facecolor='black'
			)
			mapping._add_axes2col(self.textMarker , self.shp.bbox)

		self.pointMarker = PatchCollection([mpatches.Circle(i, 0.2, ec="none") for i in centers], edgecolor='white', facecolor='black')
		mapping._add_axes2col(self.pointMarker, self.shp.bbox)

		self.datDict = []

		self.__create_main_frame()
		self.__on_draw()

	def addMap(self, map):
		'''
		This method add choropleth map to container.
		the parameter, "map" is a dictionary to describe the file, it has:

		required keys:
		'name': the map title
		'values': the values applied to choropleth map
		'keys': corresponding keys to describe the values
		'key_name': the name of the key in the table of dbf file, with which the values are joined to shape file

		optional keys:
		'color': default color scheme of the map
		'k': default classification level of the map
		'classification': default classification method applied to the map
		'description': a line shows with map
		'font_size':font size for legends' labels
		'''
		self.datDict += [map]
		self.cbbMapName.addItem(map['name'])

	def addMaps(self, maps):
		self.datDict += maps		
		self.cbbMapName.addItems([i['name'] for i in maps])

	def __plot_unique_values_map(self, values, colorsindex):
		map_obj = self.map

		pvalues = mapping._expand_values(values, map_obj.shp2dbf_row)
		map_obj.set_color([colorsindex[j] for j in pvalues])
		map_obj.set_edgecolor('k')
		return map_obj

	def __plot_fmpt(self, ax, title, v, keys, kname, k, cls, clr):
		index = dict(zip(keys, v))
		values = np.array([index[i] for i in self.dbf.by_col[kname] if i in index])

		#if sum([values[0] != i for i in values]) == 0: #when all the values are same
		#	chorop = mapping.base_choropleth_unique(self.map, values, cmap=clr)
		if cls == 'self_defined_unique':
			chorop = self.__plot_unique_values_map(values, clr)
			clr_list = clr.values()
			clr_list.reverse()
			cmap = matplotlib.colors.ListedColormap(clr_list)
			matplotlib.colorbar.ColorbarBase(self.axLegend, cmap=cmap, ticks=[])
			for j, lab in enumerate(k):
			    self.axLegend.text(1.1, (len(k) - j - .5) / len(k), lab, ha='left', va='center')
		else:
			chorop = mapping.base_choropleth_classif(self.map, values, k=k, classification=cls, cmap=clr)
			boundaries = np.round(chorop.norm.boundaries, decimals=3)			
			cmap = chorop.get_cmap()
			norm = chorop.norm
			matplotlib.colorbar.ColorbarBase(self.axLegend, cmap=cmap, norm=norm, boundaries=boundaries, ticks=boundaries, orientation='vertical')

		mapping.setup_ax([chorop] + (self.textMarker is None and [] or [self.textMarker]), ax)
		ax.set_title(title)
	
	def __on_draw(self):
		self.axMap.clear()		
		self.axLegend.clear()
		self.axDescription.clear()

		try:
			mi = self.cbbMapName.currentIndex()
			if self.cbbCN.isEnabled():
				cn  = int(self.cbbCN.currentText())
			else:
				cn = self.datDict[mi]['k']
			if self.cbbColors.isEnabled():
				clr = self.cbbColors.currentText()
			else:
				clr = self.datDict[mi]['color']
		except:
			return None

		if(len(self.datDict) == 0):
			return None

		matplotlib.rcParams.update({'font.size': 12})
		title = self.datDict[mi]['name']
		values= self.datDict[mi]['values']
		keys  = self.datDict[mi]['keys']
		kname = self.datDict[mi]['key_name']
		clas  = 'quantiles'

		if('classification' in self.datDict[mi]):
			clas = self.datDict[mi]['classification']
		if('font_size' in self.datDict[mi]):
			matplotlib.rcParams.update({'font.size': self.datDict[mi]['font_size']})

		self.__plot_fmpt(self.axMap, title, values, keys, kname, cn, clas, clr)

		if('description' in self.datDict[mi]):
			self.axDescription.set_title(self.datDict[mi]['description'])

		self.canvas.draw()
	
	def __set_selection(self, cbb, text):
		index = cbb.findText(text)
		if(index != -1):
			cbb.setCurrentIndex(index)

	def __on_map_change(self):
		self.cbbCN.currentIndexChanged.disconnect()
		self.cbbColors.currentIndexChanged.disconnect()
		self.cbbCN.setEnabled(True)
		self.cbbColors.setEnabled(True)

		mi = self.cbbMapName.currentIndex()
		if ('classification' in self.datDict[mi]) and (self.datDict[mi]['classification'] == 'self_defined_unique'):
			self.cbbCN.setEnabled(False)
			self.cbbColors.setEnabled(False)
		else:
			if ('k' in self.datDict[mi]):
				self.__set_selection(self.cbbCN, str(self.datDict[mi]['k']))
			if ('color' in self.datDict[mi]):
				self.__set_selection(self.cbbColors, self.datDict[mi]['color'])

		self.__on_draw()

		self.cbbCN.currentIndexChanged.connect(self.__on_draw)
		self.cbbColors.currentIndexChanged.connect(self.__on_draw)

	def __init_blank_axes(self, r, c, i):
		ax = self.fig.add_subplot(r, c, i)
		ax.set_frame_on(False)
		ax.axes.get_yaxis().set_visible(False)
		ax.axes.get_xaxis().set_visible(False)
		return ax
	
	def __create_main_frame(self):
		self.main_frame = QWidget()
		self.dpi = 100
		self.fig = Figure((9, 4), dpi=self.dpi)
		self.canvas = FigureCanvas(self.fig)
		self.canvas.setParent(self.main_frame)		
		self.canvas.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

		self.mpl_toolbar = NavigationToolbar(self.canvas, self.main_frame)

		self.axMap = self.fig.add_subplot(111)
		self.axLegend = self.fig.add_subplot(2,8,16)
		self.axDescription = self.__init_blank_axes(64,3,190)
		
		self.cbbMapName = QComboBox()
		self.cbbMapName.currentIndexChanged.connect(self.__on_map_change)
		self.cbbCN = QComboBox()
		self.cbbCN.currentIndexChanged.connect(self.__on_draw)
		self.cbbCN.addItems([str(i) for i in xrange(2, 10)])
		self.cbbColors = QComboBox()
		self.cbbColors.currentIndexChanged.connect(self.__on_draw)
		self.cbbColors.addItems([
			'Blues', 'BuGn', 'BuPu', 'GnBu', 'Greens', 
			'Greys', 'Oranges', 'OrRd', 'PuBu', 'PuBuGn', 
			'PuRd', 'Purples', 'RdPu', 'Reds', 'YlGn',
			'YlGnBu', 'YlOrBr', 'YlOrRd'
		])

		hbox = QHBoxLayout()		

		widgetList = [
				QLabel('Map:'),
				self.cbbMapName
		]
		[(hbox.addWidget(w), hbox.setAlignment(w, Qt.AlignVCenter)) for w in widgetList]

		hbox.addStretch(100)

		widgetList = [
				QLabel('Classification Level:'),
				self.cbbCN,
				QLabel('    Color Scheme:'),
				self.cbbColors
		]
		[(hbox.addWidget(w), hbox.setAlignment(w, Qt.AlignVCenter)) for w in widgetList]
		
		vbox = QVBoxLayout()
		vbox.addLayout(hbox)
		vbox.addWidget(self.canvas)
		vbox.addWidget(self.mpl_toolbar)
		
		self.main_frame.setLayout(vbox)
		self.setCentralWidget(self.main_frame)
#----------------------------------------------------------------------------------
#----------------------------------------------------------------------------------
class RankMarkov:
	'''
	This class generates and holds the matrices of full rank Markov and geographical
	rank Markov. It also provide the function to do the spatial dynamics test. And 
	the class will holds the rusults of the test.
	'''
	def __init__(self, filePath, timeRes = 1):
		self.__fp = filePath
		self.__f = None
		self._rank_matrix = None
		self._f_trans_mat = None
		self._f_fmpt_mat = None		
		self._g_trans_times_mat = None
		self._g_trans_mat = None
		self._g_fmpt_mat = None
		self._sym_trans_mat = None
		self._rmt_results = None
		self._rmt_file = 'RankMarkovTest.csv'
		self._rmi_results = None
		self._rmi_at_results = None
		self._time_resolution = timeRes
	
	@property
	def _file(self):
		'''
		property: retrieve the file object of CSV data file
		'''
		if self.__f is None:
			self.__f = pysal.open(self.__fp)
		return self.__f

	@property 
	def geoUnits(self):
		'''
		property: retrieve the list of region names in data file
		'''
		return self._file.header[1:]

	@property
	def rankMat(self):
		'''
		property: retrieve the rank matrix
		'''
		if self._rank_matrix is None:
			self._rank_matrix = self._rankMatrix()
		return self._rank_matrix

	@property
	def fTransMat(self):
		'''
		property: retrieve the transition matrix of full rank Markov
		'''
		if self._f_trans_mat is None:
			self._fullRankMarkov()
		return self._f_trans_mat

	@property
	def fFMPTMat(self):
		'''
		property: retrieve the first mean passage times matrix of full rank Markov
		'''
		if self._f_fmpt_mat is None:
			self._f_fmpt_mat = (pysal.ergodic.fmpt(np.matrix(self.fTransMat)) * self._time_resolution).tolist()
		return self._f_fmpt_mat

	@property
	def gTransMat(self):
		'''
		property: retrieve the transition matrix of geographical rank Markov
		'''
		if self._g_trans_mat is None:
			self._geoRankMarkov()
		return self._g_trans_mat

	@property
	def gTransTimesMat(self):
		'''
		property: retrieve the raw transition matrix of geographical rank Markov, 
		which isn't divided by the number of columns.
		'''
		if self._g_trans_times_mat is None:
			self._geoRankMarkov()
		return self._g_trans_times_mat

	@property
	def symTransMat(self):
		'''
		property: retrieve the symmetric transition matrix of geographical rank Markov
		'''
		if self._sym_trans_mat is None:
			self._geoRankMarkov()
		return self._sym_trans_mat

	@property
	def gFMPTMat(self):
		'''
		property: retrieve the first mean passage times matrix of geographical rank Markov
		'''
		if self._g_fmpt_mat is None:
			self._g_fmpt_mat = (pysal.ergodic.fmpt(np.matrix(self.gTransMat)) * self._time_resolution).tolist()
		return self._g_fmpt_mat

	def _rankMatrix(self):
		'''
		This method converts the raw data to a rank matrix
		'''
		data = np.array([self._file.by_col[y] for y in self.geoUnits]).transpose()
		ranks = np.array([pysal.Quantiles(y, len(self.geoUnits)).yb for y in data])
		#reverse all ranks, make the higher ranks represent the greater values
		for i in xrange(ranks.shape[0]):
			for j in xrange(ranks.shape[1]):
				ranks[i, j] = len(self.geoUnits) - 1 - ranks[i, j]
		return ranks

	def _fullRankMarkov(self):
		'''
		This method generates the transition matrix using full rank Markov
		'''
		markov = pysal.Markov(self.rankMat.transpose())
		self._f_trans_mat = markov.p.tolist()

	def _geoRankMarkov(self):
		'''
		This method generates the transition matrix, raw transition matrix, and 
		symmetric transition matrix using geographical rank Markov
		'''
		rm = self.rankMat
		nstates = len(self.geoUnits)

		#generate raw transition matrix and symmetric transition matrix together
		transMatrix = np.zeros((nstates, nstates))
		self._sym_trans_mat = np.zeros((nstates, nstates))
		for k in xrange(len(rm) - 1):
			for i in xrange(nstates):
				for j in xrange(nstates):
					if rm[k, i] == rm[k+1, j]:
						transMatrix[i, j] += 1
						if rm[k, j] == rm[k+1, i]:
							self._sym_trans_mat[i, j] += 1
						break
		self._g_trans_times_mat = copy.copy(transMatrix.tolist())
		
		#generate transition matrix
		for i in transMatrix:
			total = sum(i)
			for j in xrange(len(i)):
				i[j] /= float(total)
		self._g_trans_mat = transMatrix.tolist()

	def _randList(self):
		'''
		This method makes a list, in which numbers from 1 to n, n is he number of regions
		in data file, are randomly distributed.
		'''
		n = self.geoUnits
		ret = np.random.rand(n)
		ret = pysal.Quantiles(ret, n)
		return ret

	def _randTransMat(self, timeSpan):
		'''
		this method makes two random matrices, one is of transition matrix, one is of 
		symmetric transition matrix.
		'''
		nstates = len(self.geoUnits)

		#initial two zero matrix 
		transMatrix = np.zeros((nstates,nstates), dtype=int)
		symTransMat = np.zeros((nstates,nstates), dtype=int)

		#timeSpan means the time range in data you provides, for instance the annual data
		#from 1929-2009, the timeSpan=80. It decides how many times we simulate the inter-
		#regional transition
		for n in xrange(timeSpan):
			rs = range(nstates)
			cs = range(nstates)
			index = [0] * nstates
			for i in xrange(nstates - 1, -1, -1):
				#r is for the code of a region that the transition start from in time t
				#c is for the code of a region that the transition end with in time t+1
				r = rs[np.random.random_integers(0, i)]
				rs.remove(r)
				c = cs[np.random.random_integers(0, i)]
				cs.remove(r)
				transMatrix[r, c] += 1
				index[r] = c
				if index[c] == r:
					symTransMat[c, r] += 1
					symTransMat[r, c] += 1
		return {'asym':transMatrix, 'sym':symTransMat}

	@staticmethod
	def _localRankMarkovTest(ttm, nb, normalize=False):
		'''
		This method calculate all the result of local spatial dynamics test, 
		using transition matrix and neighbor data from spatial weight. The 
		results are held in a list.
		'''
		return np.array([sum([ttm[i][j] for j in nb[i]]) / (normalize and len(nb[i]) or 1) for i in xrange(len(ttm))])

	def _readRMT(self):
		'''
		This method gets the result of spatial dynamics test from file
		'''
		if not os.path.isfile(self._rmt_file):
			return None
		f = pysal.open(self._rmt_file)
		return dict(zip(f.header, [f[:,i] for i in xrange(len(f.header))]))

	def _writeRMT(self):
		'''
		This method writes the result of spatial dynamics test to file
		'''
		n = len(self._rmt_results)

		out = WriteFile(self._rmt_file)
		print (("%s," * n)[:-1]) % tuple(self._rmt_results.keys())
		vs = self._rmt_results.values()
		for i in np.array(vs).transpose():
			print (("%s," * n)[:-1]) % tuple(i)
		out.end()

	def sortWeights(self, shpName, joinKeyName):
		n = len(self.geoUnits)

		sw = pysal.rook_from_shapefile(shpName + '.shp')
		dbf = pysal.open(shpName + '.dbf')
		index = dict(zip(self.geoUnits, xrange(n)))
		trans = [index[i] for i in dbf.by_col[joinKeyName] if i in index]
		_nb = sw.neighbors
		nb = dict(zip(trans, [[trans[j] for j in i] for i in _nb.values()]))
		nb_miss = [(i, []) for i in xrange(n) if i not in nb]
		nb = sorted(nb.items() + nb_miss)
		return pysal.W(dict(nb))
	
	def rankMarkovTest(self, weights, simTimes=999, tryFromFile=True):
		'''
		This method do the spatial dynamics test using Monte Carlo method
		and four test standards, origin based, destination based, symmetric,
		and hybrid.
		'''
		if tryFromFile:
			self._rmt_results = self._readRMT()
		if self._rmt_results is not None:
			return

		n = len(self.geoUnits)

		#get the spatial weight information
		nb = weights.neighbors.values()

		timeSpan = int(sum(self.gTransTimesMat[0]))
		
		#get the result of local spatial dynamics test based on origin
		#based standard
		ttm = self.gTransTimesMat
		pv_ori  = np.array([1.0] * n)
		rmt_ori = RankMarkov._localRankMarkovTest(ttm, nb)

		#get the result of local spatial dynamics test based on destinaiton
		#based standard
		ttm = np.array(ttm).transpose()
		pv_dst  = np.array([1.0] * n)
		rmt_dst = RankMarkov._localRankMarkovTest(ttm, nb)

		#get the result of local spatial dynamics test based on symmetric
		#standard
		pv_sym = np.array([1.0] * n)
		rmt_sym = RankMarkov._localRankMarkovTest(self.symTransMat, nb)

		#get the result of local spatial dynamics test based on hybrid
		#standard
		pv_hyb  = np.array([1.0] * n)
		rmt_hyb = rmt_dst + rmt_ori - rmt_sym

		#get the global test results based on different standards
		globalRMT = sum(rmt_ori)
		pv_globle = 1.0
		globalRMT_sym = sum(rmt_sym)
		pv_globle_sym = 1.0
		globalRMT_hyb = sum(rmt_hyb)
		pv_globle_hyb = 1.0

		np.random.seed(512)

		#simulation process
		for i in xrange(simTimes):
			rtm = self._randTransMat(timeSpan)
			sym = rtm['sym']
			asym = rtm['asym']

			rtl_o = RankMarkov._localRankMarkovTest(asym, nb)
			rtl_d = RankMarkov._localRankMarkovTest(asym.transpose(), nb)
			rtl_s = RankMarkov._localRankMarkovTest(sym, nb)
			rtl_h = rtl_o + rtl_d - rtl_s

			pv_ori += rtl_o > rmt_ori
			pv_dst += rtl_d > rmt_dst
			pv_sym += rtl_s > rmt_sym
			pv_hyb += rtl_h > rmt_hyb

			pv_globle += sum(rtl_o) > globalRMT
			pv_globle_sym += sum(rtl_s) > globalRMT_sym
			pv_globle_hyb += sum(rtl_h) > globalRMT_hyb

			sys.stdout.write("Rank-Markov-based Test, simulating using Monte Carlo Method: [%2d%%]\r" % ((i + 1) * 100 / simTimes))
			sys.stdout.flush()
		pv_ori /= simTimes + 1
		pv_dst /= simTimes + 1
		pv_sym /= simTimes + 1
		pv_hyb /= simTimes + 1
		pv_globle /= simTimes + 1
		pv_globle_sym /= simTimes + 1
		pv_globle_hyb /= simTimes + 1

		#organize all the result in to dictionary for writing to file 
		self._rmt_results = collections.OrderedDict([
			('state', self.geoUnits),
			('rmt_ori', rmt_ori),
			('pv_ori' , pv_ori),
			('rmt_dst', rmt_dst),
			('pv_dst' , pv_dst),
			('rmt_sym', rmt_sym),
			('pv_sym' , pv_sym),
			('rmt_hyb', rmt_hyb),
			('pv_hyb' , pv_hyb),
			('global' , [str(globalRMT)] + [''] * (n - 1)),
			('p-value', [str(pv_globle)] + [''] * (n - 1)),
			('global_s' , [str(globalRMT_sym)] + [''] * (n - 1)),
			('p-value_s', [str(pv_globle_sym)] + [''] * (n - 1)),
			('global_h' , [str(globalRMT_hyb)] + [''] * (n - 1)),
			('p-value_h', [str(pv_globle_hyb)] + [''] * (n - 1))
		])

		#write result to file
		self._writeRMT()

		#for i in zip(xrange(n), self.geoUnits, rmt_ori, pv_ori, rmt_dst, pv_dst, rmt_hyb, pv_hyb):
			#print "%2d %s %2.0f %5.3f %2.0f %5.3f %2.0f %5.3f" % i
		#print "GR=%d	p-value=%5.3f" % (globalRMT, pv_globle)

	def rankMoranITest(self, weights):
		'''
		This method do the spatial dynamic test using Local Moran's I 
		'''
		TotalTimes = np.array([sum(i) for i in self.gTransTimesMat])
		SojournTimes = np.array([self.gTransTimesMat[i][i] for i in xrange(len(self.geoUnits))])
		TransitionTimes = TotalTimes - SojournTimes
		ml = pysal.Moran_Local(TransitionTimes, weights)
		self._rmi_results = {
			'local': (ml.p_sim <= .05) * ml.q,
			'global': pysal.esda.moran.Moran(TransitionTimes, weights),
			'stat': r'$y_i=\sum_{j=0}^n\sum_{t=1}^{T-1}\Omega_{i,j}^{t,t+1}-\sum_{t=1}^{T-1}\Omega_{i,i}^{t,t+1}$'
		}

	def rankMoranITest_AT(self, weights):
		'''
		This method do the spatial dynamic test using Local Moran's I
		to find the clusters that more likely give its ranks to its neighbors
		'''
		AdjacentTransition = RankMarkov._localRankMarkovTest(self.gTransTimesMat, weights.neighbors.values())
		ml = pysal.Moran_Local(AdjacentTransition, weights)
		self._rmi_at_results = {
			'local': (ml.p_sim <= .05) * ml.q,
			'global': pysal.esda.moran.Moran(AdjacentTransition, weights),
			'stat': r'$y_i=\sum_{j=0}^n\sum_{t=1}^{T-1}W_{i,j}\Omega_{i,j}^{t,t+1}$'
		}


	def printFullRankMarkov(self, header = None):
		'''
		This method prints all the matrices of full rank Markov
		'''
		print "Full Rank Probability Transition Matrix:"
		printMatrix(self.fTransMat, 6, 4, header)
		print		

		print "First Mean Passage Times Matrix:"
		printMatrix(self.fFMPTMat, 6, 0, header)

	def printGeoRankMarkov(self, header = None):
		'''
		This method prints all the matrices of geographical rank Markov
		'''
		print "Geograhic Rank Markov Overall Migration Times Matrix:"
		printMatrix(self.gTransTimesMat, 2, 0, header)
		print

		print "Geograhic Rank Markov Transition Matrix:"
		printMatrix(self.gTransMat, 6, 4, header)
		print

		print "First Mean Passage Times Matrix:"
		printMatrix(self.gFMPTMat, 6, 0, header)

	def generateFMPTMaps(self, datFile = None):
		'''
		This method generates all the FMPT maps
		'''
		if datFile is not None and os.path.isfile(datFile):
			dat = pysal.open(datFile)
			cols= dat.by_col
			rows= dict([(i[0], i[1:-1]) for i in dat.by_row])
			sas = dat.header[1:-1] 
		else:
			sas = self.geoUnits
			fm  = self.gFMPTMat
			cols= dict(zip(sas, np.array(fm).transpose().tolist()))
			rows= dict(zip(sas, fm))

		return [{
			'name':'First Mean Passage Time ' + j + ' ' + saDict[i],
			'values':j=='from' and rows[i] or cols[i],
			'keys':sas,
			'key_name':'STATE_ABBR',
			'color':'YlGnBu',
			'k':6
		} for i in sas for j in ('from', 'to')]
	
	def generateRankMoranIMap(self):
		'''
		This method generates local spatial dynamics test maps using
		local Moran's I
		'''
		resultList = [self._rmi_results, self._rmi_at_results]
		validation = True

		for i in resultList:
			validation = validation and (i is not None)

		if not validation:
			return None

		rr = self._rmi_results
		return [{
			'name':'Local Spatial Rank Dynamics (Using Local Moran\'s I, origin based)',
			'values':rr['local'],
			'keys':self.geoUnits,
			'key_name':'STATE_ABBR',
			'color':{
				0:(1, 1, .8, 1), #white for not significant(0) units
				1:(.9, 0, 0, 1), #red for HH(1) units
				2:(.9, .9, 0, 1), #yellow for LH(2) units
				3:(0, 0, .9, 1), #blue for LL(3) units
				4:(0, .9, 0, 1)  #green for HL(4) units
			},
			'k':['Non-significant','High-high','Low-High','Low-low','High-Low'],
			'classification':'self_defined_unique',
			'font_size':9,
			'description':'%s\n$Global I=%.3f$\n$p-value=%.3f$' % (rr['stat'], rr['global'].I, rr['global'].p_norm)
		} for rr in resultList]

	def generateRMTMaps(self):
		'''
		This method generates local spatial dynamics test maps under four
		standards
		'''
		if self._rmt_results is None:
			return None

		rr = self._rmt_results
		return [{
			'name':'Local Spatial Rank Dynamics (' + j + ')',
			'values':[n <= .05 for n in rr[i]],
			'keys':rr['state'],
			'key_name':'STATE_ABBR',
			'color':{
				0:(1, 1, .7, 1), #white for not significant(0) units
				1:(.7, 0, 0, 1), #red for HH(1) units
			},
			'k':['Non-significant','Significant'],
			'classification':'self_defined_unique',
			'font_size':9,
			'description':
				j == 'symmetric' and 'GR*=%.3f\np-value=%.3f' % (float(rr['global_s'][0]), float(rr['p-value_s'][0])) or
				j == 'hybrid'    and 'GR**=%.3f\np-value=%.3f' % (float(rr['global_h'][0]), float(rr['p-value_h'][0])) or
				'GR=%.3f\np-value=%.3f' % (float(rr['global'][0]), float(rr['p-value'][0]))				
		} for i, j in zip(['pv_ori', 'pv_dst', 'pv_sym', 'pv_hyb'], ['origin based', 'destination based', 'symmetric', 'hybrid'])]
#----------------------------------------------------------------------------------
#----------------------------------------------------------------------------------
class WriteFile:
	'''
	This class helps print content to files instead of to screen.
	Create an object to start, and call end() function to stop.
	'''
	def __init__(self, filepath, mode='w'):
		self.file = open(filepath, mode)
		self.oldStdout = sys.stdout
		sys.stdout = self.file
	def end(self):
		self.file.close()
		sys.stdout = self.oldStdout

def printMatrix(matr, width, prec, header, separator = ' '): 
	'''
	This method formats the matrices' output
	'''
	n = len(matr[0]) 
	index = 0
	w = str(width)
	p = str(prec) 
	print ("  " + separator + ("%" + w + "s" + separator) * n) % tuple(header)
	for i in matr:
		print ("%2s" + separator + ("%" + w + "." + p + "f" + separator) * n) % ((header[index], ) + tuple(i))
		index += 1

def generateRscript(inname, plotout, histout):
	scriptTemplate = '''
library(spatstat)
t = read.table("%(in)s")
m = data.matrix(t)

cols = paste(c("#552299"), as.hexmode(c(1:16*16-1, rep(255, 12))), sep="")
png(file="%(out1)s", width=800, height=800)
plot(im(m), col = cols, cex.main=3, main="first mean passage time of U.S. Population")
dev.off()

n = 200
png(file="%(out2)s")
hist(m, col='blue', border='blue', main='FMPT Rank to Rank', cex.main=1.5, breaks=c(0:n*ceiling(max(m)/n)))
dev.off()
	'''
	print scriptTemplate % {'in':inname, 'out1':plotout, 'out2':histout}
#----------------------------------------------------------------------------------
#----------------------------------------------------------------------------------
saDict = {
	'AL':'Alabama',
	'AK':'Alaska',
	'AZ':'Arizona',
	'AR':'Arkansas',
	'CA':'California',
	'CO':'Colorado',
	'CT':'Connecticut',
	'DE':'Delaware',
	'DC':'District of Columbia',
	'FL':'Florida',
	'GA':'Georgia',
	'HI':'Hawaii',
	'ID':'Idaho',
	'IL':'Illinois',
	'IN':'Indiana',
	'IA':'Iowa',
	'KS':'Kansas',
	'KY':'Kentucky',
	'LA':'Louisiana',
	'ME':'Maine',
	'MD':'Maryland',
	'MA':'Massachusetts',
	'MI':'Michigan',
	'MN':'Minnesota',
	'MS':'Mississippi',
	'MO':'Missouri',
	'MT':'Montana',
	'NE':'Nebraska',
	'NV':'Nevada',
	'NH':'New Hampshire',
	'NJ':'New Jersey',
	'NM':'New Mexico',
	'NY':'New York',
	'NC':'North Carolina',
	'ND':'North Dakota',
	'OH':'Ohio',
	'OK':'Oklahoma',
	'OR':'Oregon',
	'PA':'Pennsylvania',
	'RI':'Rhode Island',
	'SC':'South Carolina',
	'SD':'South Dakota',
	'TN':'Tennessee',
	'TX':'Texas',
	'UT':'Utah',
	'VT':'Vermont',
	'VA':'Virginia',
	'WA':'Washington',
	'WV':'West Virginia',
	'WI':'Wisconsin',
	'WY':'Wyoming'
}
#----------------------------------------------------------------------------------
#----------------------------------------------------------------------------------
fmptCSVName  = "g_fmpt.csv"

def process(datSrcFile, timeRes=1):
	rm = RankMarkov(datSrcFile, timeRes)
	header  = rm.geoUnits
	rankProcess  = {'f':rm.printFullRankMarkov, 'g':rm.printGeoRankMarkov}
	fmptMatrices = {'f':rm.fFMPTMat, 'g':rm.gFMPTMat}
	outDataName  = {'f':"f_markov.txt", 'g':"g_markov.txt"}
	fmptDataName = {'f':"f_fmpt.dat", 'g':"g_fmpt.dat"}
	fmptPlotName = {'f':"f_fmpt.png", 'g':"g_fmpt.png"}
	fmptHistName = {'f':"f_fmpt_hist.png", 'g':"g_fmpt_hist.png"}
	fmptHeadNames= {'f':[str(i) for i in xrange(len(header))], 'g':header} 
	rsEnginePath = r"e:\r\bin\rscript"
	rscriptName  = "__fpmt_rs"

	for n in ('f', 'g'):
		out = WriteFile(outDataName[n])
		rankProcess[n](fmptHeadNames[n])

		fmptOut = WriteFile(fmptDataName[n])
		printMatrix(fmptMatrices[n], 6, 2, fmptHeadNames[n])
		fmptOut.end()

		if(n == 'g'):
			fmptOut = WriteFile(fmptCSVName)
			printMatrix(fmptMatrices[n], 0, 2, fmptHeadNames[n], ',')
			fmptOut.end()

		rsOut = WriteFile(rscriptName, 'a')
		generateRscript(fmptDataName[n], fmptPlotName[n], fmptHistName[n])
		rsOut.end()

		out.end()

	try:
		rsprocess = sp.Popen(rsEnginePath + ' ' + rscriptName)
		rsprocess.wait()
	except:
		pass

	os.remove(rscriptName)

	print "successfully processed."
	return rm

def main():
        import os

        cwd = os.path.dirname(__file__)
        datFile = os.path.join(cwd, "data", 'us_ci.csv')
        mapFile = os.path.join(cwd, "pysal", "examples", "us48")

	os.chdir(sys.path[0])

	if not (os.path.isfile(fmptCSVName)):
		print 'Reqired result data not found, now (re)processing...\n'
		rm = process(datFile)
	else:
		rm = RankMarkov(datFile)

	w = rm.sortWeights(mapFile, 'STATE_ABBR')
	rm.rankMarkovTest(w)
	rm.rankMoranITest(w)
	rm.rankMoranITest_AT(w)

	fmptMaps = rm.generateFMPTMaps(fmptCSVName)
	rmtMaps  = rm.generateRMTMaps()
	rmiMaps  = rm.generateRankMoranIMap()

	app = QApplication(sys.argv)
	mwin = MapWindow(mapFile, 'STATE_ABBR')
	mwin.addMaps(rmiMaps)
	mwin.addMaps(rmtMaps)
	mwin.addMaps(fmptMaps)
	mwin.show()
	app.exec_()

if __name__ == "__main__":
	main()
